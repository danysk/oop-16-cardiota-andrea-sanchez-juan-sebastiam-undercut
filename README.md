# README #

## Target ##

UnderCut is a **telemetry software** commissioned by University of Bologna Formula SAE Team (UniBo Motorsport), meant for motorsport engineers to ease reading of vital parameters of the car.


## Contributors: ##

* **Andrea Cardiota**: *andrea.cardiota@studio.unibo.it*

* **Juan Sebastiam Sanchez**: *juan.sanchez2@studio.unibo.it*