package org.undercut.test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

import org.junit.Test;
import org.undercut.model.UnderCutModel;
import org.undercut.model.UnderCutModelImpl;
import org.undercut.model.instantcollection.InstantCollection;
import org.undercut.model.instantcollection.InstantCollectionImpl;
import org.undercut.model.logger.Logger;
import org.undercut.model.logger.LoggerImpl;
import org.undercut.view.utils.TabName;

/**
 * Class due to test the correct behavior of the logging system.
 */
public class LoggerTest {

    private static final String PATH_TO_LOGS_DIR = System.getProperty("user.home")
                                                    + System.getProperty("file.separator")
                                                    + "UnderCut-Logs"
                                                    + System.getProperty("file.separator");
    private static final String COMMON_LOGFILE_NAME_PART = "UnderCut-Log";
    private static final String EXTENTION = ".txt";
    private static final int TOT_LOGS = 10;

    /**
     * Basic test of log setup and file writing.
     */
    @Test
    public void basicLogFileTest() {
        final InstantCollection ic = new UnderCutModelImpl().getValues();
        final Logger log = new LoggerImpl();
        Long fileLenght;
        try {
            File file = new File(LoggerTest.PATH_TO_LOGS_DIR);
            if (!file.exists()) {
                fail("Logger should have created the folder.");
            } else if (file.listFiles() == null) {
                fail("The folder should be initially empty.");
            }
            log.setWhatToLog(Arrays.asList(TabName.ECU, TabName.VCU, TabName.INERTIAL));
            file = new File(LoggerTest.PATH_TO_LOGS_DIR + LoggerTest.COMMON_LOGFILE_NAME_PART
                            + 1 + LoggerTest.EXTENTION);
            if (!file.exists()) {
                fail("Logger should have created first log file.");
            } else {
                fileLenght = file.length();
                log.doLog(ic);
                if (fileLenght >= file.length()) {
                    fail("Logger should have written the log block.");
                }
            }
        } catch (IllegalArgumentException | IOException e) {
            fail("There shouldn't be any problems.");
        }
    }

    /**
     * Basic test of multiple log files.
     */
    @Test
    public void multipleLogFilesTest() {
        final InstantCollection ic = new UnderCutModelImpl().getValues();
        final Logger log = new LoggerImpl();
        Long fileLenght;
        try {
            for (int i = 1; i <= LoggerTest.TOT_LOGS; i++) {
                log.setWhatToLog(Arrays.asList(TabName.ECU, TabName.VCU, TabName.INERTIAL));
                final File file = new File(LoggerTest.PATH_TO_LOGS_DIR + LoggerTest.COMMON_LOGFILE_NAME_PART
                                + i + LoggerTest.EXTENTION);
                if (!file.exists()) {
                    fail("Logger should have created log file.");
                } else {
                    fileLenght = file.length();
                    log.doLog(ic);
                    if (fileLenght >= file.length()) {
                        fail("Logger should have written the log block.");
                    }
                }
            }
        } catch (IllegalArgumentException | IOException e) {
            fail("There shouldn't be any problems.");
        }
    }

    /**
     * Basic log file multiple writing test.
     */
    @Test
    public void basicDoLogTest() {
        final UnderCutModel ucModel = new UnderCutModelImpl();
        final Logger log = new LoggerImpl();
        try {
            log.setWhatToLog(Arrays.asList(TabName.ECU, TabName.VCU, TabName.INERTIAL));
            final File file = new File(LoggerTest.PATH_TO_LOGS_DIR + LoggerTest.COMMON_LOGFILE_NAME_PART
                            + 1 + LoggerTest.EXTENTION);
            if (!file.exists()) {
                fail("Logger should have created first log file.");
            } else {
                for (int i = 0; i < LoggerTest.TOT_LOGS; i++) {
                    final Long fileLenght = file.length();
                    log.doLog(ucModel.getValues());
                    if (fileLenght >= file.length()) {
                        fail("Logger should have written the log block.");
                    }
                }
            }
        } catch (IllegalArgumentException | IOException e) {
            fail("There shouldn't be any problems.");
        }
    }

    /**
     * Test of log errors handling.
     */
    @Test
    public void errorInLogTest() {
        final Logger log = new LoggerImpl();
        try {
            log.doLog(null);
            fail("Should throw IllegalArgumentException.");
        } catch (IOException e) {
            fail("TThere's a normal situation test, should not be any IO problems.");
        } catch (IllegalArgumentException e) {
            assertNotNull(e);
        }
        try {
            log.doLog(new InstantCollectionImpl());
            fail("Should throw IllegalArgumentException.");
        } catch (IOException e) {
            fail("TThere's a normal situation test, should not be any IO problems.");
        } catch (IllegalArgumentException e) {
            assertNotNull(e);
        }
    }

}
