package org.undercut.test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import org.junit.Test;
import org.undercut.model.FieldName;
import org.undercut.model.instantcollection.DefaultInstantCollection;
import org.undercut.model.instantcollection.InstantCollection;
import org.undercut.model.instantcollection.InstantCollectionImpl;
import org.undercut.view.utils.TabName;

/**
 * Class due to test the correct handling of the InstantCollection data structure.
 */
public class InstantCollectionTest {

    /**
     * Test of DefaultInstantCollection.
     */
    @Test
    public void defaultInstantCollectionTest() {
        final InstantCollection ic = DefaultInstantCollection.getDefaultInstantCollection();
        if (ic == null) {
            fail("DefaultInstantCollection couldn't be null.");
        }
        try {
            ic.getValuesOf(null);
            fail("Get value operation of a null paramater should throw a IllegalArgumentException");
        } catch (IllegalArgumentException e) {
            assertNotNull(e);
        }
        for (final TabName tabName : TabName.values()) {
            if (!tabName.equals(TabName.LOG) && (ic.getValuesOf(tabName) == null || ic.getValuesOf(tabName).isEmpty())) {
                fail("No value of the DefaultInstantCollection can be null or empty.");
            }
        }
    }

    /**
     * Basic test of an InstantCollection.
     */
    @Test
    public void basicInstantCollectionTest() {
        final InstantCollectionImpl ic = new InstantCollectionImpl();
        for (final FieldName field : FieldName.values()) {
            ic.setValueOf(field, 1.0);
            if (!ic.getValuesOf(field.getBelongsTab()).containsKey(field)) {
                fail("The InstantCollection must contain the lastly added value.");
            }
        }
        for (final TabName tabName : TabName.values()) {
            if (!tabName.equals(TabName.LOG) && ic.getValuesOf(tabName).isEmpty()) {
                fail("The sub-map of the InstantCollection couldn't be empty.");
            }
        }
    }

    /**
     * Test errors on InstantCollection.
     */
    @Test
    public void errorInstantCollectionTest() {
        final InstantCollectionImpl ic = new InstantCollectionImpl();
        try {
            ic.setValueOf(null, 1.0);
            fail("Should throw a IllegalArgumentException.");
        } catch (IllegalArgumentException e) {
            assertNotNull(e);
        }
        try {
            ic.getValuesOf(null);
            fail("Should throw a IllegalArgumentException.");
        } catch (IllegalArgumentException e) {
            assertNotNull(e);
        }
        try {
            ic.getValuesOf(TabName.LOG);
            fail("Should throw a IllegalArgumentException because LOG is not part of collection.");
        } catch (IllegalArgumentException e) {
            assertNotNull(e);
        }
    }

}
