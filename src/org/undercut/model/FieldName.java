package org.undercut.model;

import org.undercut.view.utils.TabName;

/**
 * Enumeration of all the fields that the application is able to process.
 */
public enum FieldName {

    /*                  ECU VALUES              */

    /**
     * Revolutions per minute.
     */
    RPM(TabName.ECU, "RPM", "RPM"),
    /**
     * Airbox Pressure.
     */
    AIRBOX_P(TabName.ECU, "AIRBOX_P", "Airbox Pressure"),
    /**
     * Fuel Pressure.
     */
    FUEL_P(TabName.ECU, "FUEL_P", "Fuel Pressure"),
    /**
     * Torque Request.
     */
    TORQUE_REQ(TabName.ECU, "TORQUE_REQ", "Torque Request"),
    /**
     * Throttle Pressure (%).
     */
    THR(TabName.ECU, "THR", "Throttle Pressure %"),
    /**
     * Brake Pressure (%).
     */
    BRK(TabName.ECU, "BRK", "Brake Pressure %"),
    /**
     * Oil Pressure.
     */
    OIL_P(TabName.ECU, "OIL_P", "Oil Pressure"),
    /**
     * Oil Temperature.
     */
    OIL_T(TabName.ECU, "OIL_T", "Oil Temperature"),
    /**
     * Air Temperature.
     */
    AIR_T(TabName.ECU, "AIR_T", "Air Temperature"),
    /**
     * Water Temperature.
     */
    H2O_T(TabName.ECU, "H2O_T", "Water Temperature"),
    /**
     * Gear.
     */
    GEAR(TabName.ECU, "GEAR", "Gear"),

    /*                  VCU VALUES              */
    /**
     * Front Brakes Pressure.
     */
    F_BRK_P(TabName.VCU, "F_BRK_P", "Front Brakes Pressure"),
    /**
     * Rear Brakes Pressure.
     */
    R_BRK_P(TabName.VCU, "R_BRK_P", "Rear Brakes Pressure"),
    /**
     * Front-Left Brake Temperature.
     */
    FL_BRK_T(TabName.VCU, "FL_BRK_T", "Front-Left Brake Temperature"),
    /**
     * Front-Right Brake Temperature.
     */
    FR_BRK_T(TabName.VCU, "FR_BRK_T", "Front-Right Brake Temperature"),
    /**
     * Rear-Left Brake Temperature.
     */
    RL_BRK_T(TabName.VCU, "RL_BRK_T", "Rear-Left Brake Temperature"),
    /**
     * Rear-Right Brake Temperature.
     */
    RR_BRK_T(TabName.VCU, "RR_BRK_T", "Rear-Right Brake Temperature"),
    /**
     * Front-Left Suspension Travel.
     */
    FL_SUS_TL(TabName.VCU, "FL_SUS_TL", "Front-Left Suspension Travel"),
    /**
     * Front-Right Suspension Travel.
     */
    FR_SUS_TL(TabName.VCU, "FR_SUS_TL", "Front-Right Suspension Travel"),
    /**
     * Rear-Left Suspension Travel.
     */
    RL_SUS_TL(TabName.VCU, "RL_SUS_TL", "Rear-Left Suspension Travel"),
    /**
     * Rear-Right Suspension Travel.
     */
    RR_SUS_TL(TabName.VCU, "RR_SUS_TL", "Rear-Right Suspension Travel"),
    /**
     * Battery Voltage.
     */
    BTR_V(TabName.VCU, "BTR_V", "Battery Voltage"),
    /**
     * Battery Current.
     */
    BTR_C(TabName.VCU, "BTR_C", "Battery Current"),
    /**
     * Current Regulator.
     */
    C_REG(TabName.VCU, "C_REG", "Current Regulator"),
    /**
     * Front-Left Wheel Speed.
     */
    FL_WH_SP(TabName.VCU, "FL_WH_SP", "Front-Left Wheel Speed"),
    /**
     * Front-Right Wheel Speed.
     */
    FR_WH_SP(TabName.VCU, "FR_WH_SP", "Front-Right Wheel Speed"),
    /**
     * Rear-Left Wheel Speed.
     */
    RL_WH_SP(TabName.VCU, "RL_WH_SP", "Rear-Left Wheel Speed"),
    /**
     * Rear-Right Wheel Speed.
     */
    RR_WH_SP(TabName.VCU, "RR_WH_SP", "Rear-Right Wheel Speed"),
    /**
     * Brake BIAS.
     */
    BRK_B(TabName.VCU, "BRK_B", "Brake Balance"),
    /**
     * Slip Left.
     */
    SLIP_L(TabName.VCU, "SLIP_L", "Rear Slip Left Differential"),
    /**
     * Slip Right.
     */
    SLIP_R(TabName.VCU, "SLIP_R", "Rear Slip Right Differential"),
    /**
     * Chassis Temperature.
     */
    CHSS_T(TabName.VCU, "CHSS_T", "Chassis Temperature"),
    /**
     * Steering Angle.
     */
    STR_ANG(TabName.VCU, "STR_ANG", "Steering Angle"),
    /**
     * Steering Angle.
     */
    ENG_T(TabName.VCU, "ENG_T", "Engine Temperature"),

    /*               INERTIAL VALUES              */
    /**
     * Gyroscope X Axis.
     */
    GYRO_X(TabName.INERTIAL, "GYRO_X", "Gyroscope X Axis"),
    /**
     * Gyroscope Y Axis.
     */
    GYRO_Y(TabName.INERTIAL, "GYRO_Y", "Gyroscope Y Axis"),
    /**
     * Gyroscope Z Axis.
     */
    GYRO_Z(TabName.INERTIAL, "GYRO_Z", "Gyroscope Z Axis"),
    /**
     * Acceleration X.
     */
    ACC_X(TabName.INERTIAL, "ACC_X", "Acceleration X"),
    /**
     * Acceleration Y.
     */
    ACC_Y(TabName.INERTIAL, "ACC_Y", "Acceleration Y"),
    /**
     * Acceleration Z.
     */
    ACC_Z(TabName.INERTIAL, "ACC_Z", "Acceleration Z");

    private final TabName belongsTab;
    private final String name;
    private final String spec;

    FieldName(final TabName tabName, final String fieldName, final String tabSpec) {
        this.belongsTab = tabName;
        this.name = fieldName;
        this.spec = tabSpec;
    }

    /**
     * Method to get the belonging tab of this field.
     * 
     * @return
     *          The belonging tab.
     */
    public TabName getBelongsTab() {
        return this.belongsTab;
    }

    /**
     * Method to get the name of this field.
     * 
     * @return
     *          Field name in a String form.
     */
    public String getName() {
        return this.name;
    }

    /**
     * Method to get a string that describes the field.
     * 
     * @return
     *          Explanatory string which describes the field.
     */
    public String toString() {
        return this.spec;
    }

}
