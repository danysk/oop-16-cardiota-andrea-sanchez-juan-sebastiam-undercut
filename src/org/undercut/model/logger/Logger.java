package org.undercut.model.logger;

import java.io.IOException;
import java.util.List;

import org.undercut.model.instantcollection.InstantCollection;
import org.undercut.view.utils.TabName;

/**
 * Interface that models the concept of a logging unit.
 */
public interface Logger {

    /**
     * Method to set the tabs to log.
     * 
     * @param tabsToLog
     *          Tabs to be logged
     *
     * @throws IllegalArgumentException
     *          If the argument is null
     * 
     * @throws IOException
     *          If there was a problem during file's preparation
     */
    void setWhatToLog(List<TabName> tabsToLog) throws IllegalArgumentException, IOException;

    /**
     * Method to write a line of data in the final log file.
     * 
     * @param dataToLog
     *          Data to be written
     *
     * @throws IllegalArgumentException
     *          If argument is null
     *
     * @throws IOException
     *          If there was a problem during log file's writting
     */
    void doLog(InstantCollection dataToLog) throws IllegalArgumentException, IOException;
}
