package org.undercut.model.datafetcher;

import java.io.IOException;
import java.util.Map;
import org.undercut.model.FieldName;

/**
 * Interface that models the concept of a unit in charge of retrieving data.
 */
public interface DataFetcher {

    /**
     * Method to get a raw collection of data from the files.
     * 
     * @return
     *          A map that contains one entry for each field (FieldName, FieldValue)
     *
     * @throws IllegalStateException
     *          Exception due to a problem during the file reading
     *
     * @throws NumberFormatException
     *          Exception due to a problem during the number casting
     *
     * @throws IOException
     *          Exception due to a problem during input streams reset
     */
    Map<FieldName, ? extends Number> getValues() throws IllegalStateException, NumberFormatException, IOException;

    /**
     * Method to reset input streams when the application is stopped.
     * 
     * @throws IOException
     *          Exception due to a problem while closing the input streams
     */
    void reset() throws IOException;

}
