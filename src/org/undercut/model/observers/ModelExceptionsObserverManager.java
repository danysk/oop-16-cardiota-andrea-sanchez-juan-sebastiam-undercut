package org.undercut.model.observers;

import org.undercut.model.utils.CheckIllegalArgument;

/**
 * Main class to define the Observer behaviour. 
 */
public class ModelExceptionsObserverManager {

    private GenericExceptionObserver exceptionObserver;

    /**
     * Method to add an exception observer to the model.
     * 
     * @param obs
     *          The observer
     *
     * @throws NullPointerException
     *          If the observer to set is null
     */
    public void addExceptionObserver(final GenericExceptionObserver obs) {
        this.exceptionObserver = obs;
    }

    /**
     * Method to notify model's exception observer.
     * 
     * @param exception
     *          The exception
     * 
     * @param exceptionMessage
     *          The exception message to warn the user with
     * 
     * @throws IllegalArgumentException
     *          If this method is called before the observer initialisation
     */
    public void notifyExceptionObserver(final Exception exception, final String exceptionMessage) 
            throws IllegalArgumentException {
        CheckIllegalArgument.checkForIllegalArgument(this.exceptionObserver);
        this.exceptionObserver.update(this, exception, exceptionMessage);
    }

}
