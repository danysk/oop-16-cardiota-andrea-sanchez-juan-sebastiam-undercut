package org.undercut.model.instantcollection;

import java.util.HashMap;
import java.util.Map;

import org.undercut.model.FieldName;
import org.undercut.model.utils.CheckIllegalArgument;
import org.undercut.view.utils.TabName;

/**
 * Class to model the concept of a collection of values relative to a certain given time.
 */
public abstract class AbstractInstantCollection implements InstantCollection {

    private final Map<TabName, Map<FieldName, Number>> instantCollection;

    /**
     * Class constructor.
     * */
    public AbstractInstantCollection() {
        this.instantCollection = new HashMap<>();
    }

    @Override
    public Map<FieldName, ? extends Number> getValuesOf(final TabName tabName) throws IllegalArgumentException {
        CheckIllegalArgument.checkForIllegalArgument(tabName);
        return this.instantCollection.get(tabName);
    }

    /**
     * Method to access the InstantCollection.
     * 
     * @return
     *          The InstantCollection object
     * */
    protected Map<TabName, Map<FieldName, Number>> getInstantCollection() {
        return this.instantCollection;
    }

}
