package org.undercut.model.instantcollection;

import java.util.HashMap;

import org.undercut.model.FieldName;
import org.undercut.view.utils.TabName;

/**
 * Class to provide a default InstantCollection (InstantCollection initialised with 0 values).
 */
public final class DefaultInstantCollection extends AbstractInstantCollection {

    private static final Double ZERO = 0.0;
    private static final DefaultInstantCollection SINGLETON = new DefaultInstantCollection();

    /**
     * Private class constructor of the DefaultInstantCollection class.
     * This class fulfills the Singleton pattern.
     */
    private DefaultInstantCollection() {
        super();
        for (final TabName tabName : TabName.values()) {
            if (!tabName.equals(TabName.LOG)) {
                super.getInstantCollection().put(tabName, new HashMap<>());
            }
        }
        for (final FieldName fieldName : FieldName.values()) {
            super.getInstantCollection().get(fieldName.getBelongsTab()).put(fieldName, DefaultInstantCollection.ZERO);
        }
    }

    /**
     * The only access method to the DefaultInstantCollection object.
     * 
     * @return
     *          The default Instant Collection
     */
    public static DefaultInstantCollection getDefaultInstantCollection() {
        return DefaultInstantCollection.SINGLETON;
    }

}
