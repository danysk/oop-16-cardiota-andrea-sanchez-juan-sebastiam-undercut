package org.undercut.model.processingengine;

import java.util.Map;
import org.undercut.model.FieldName;
import org.undercut.model.instantcollection.InstantCollection;

/**
 * Interface to models the concept of an elaboration unit able to process data according to a certain logic.
 */
public interface ProcessingEngine {

    /**
     * Method to process data in an organised InstantCollection.
     * 
     * @param values
     *          Map that contains all the raw data
     * 
     * @return
     *          InstantCollection that contains all the elaborated values in an organised form
     * 
     * @throws IllegalArgumentException
     *          If argument is null or empty
     */
    InstantCollection processData(Map<FieldName, ? extends Number> values) throws IllegalArgumentException;

}
