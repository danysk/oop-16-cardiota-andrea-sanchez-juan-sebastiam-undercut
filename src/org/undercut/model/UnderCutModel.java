package org.undercut.model;

import java.io.IOException;
import java.util.List;

import org.undercut.model.instantcollection.InstantCollection;
import org.undercut.model.observers.GenericExceptionObserver;
import org.undercut.view.utils.TabName;

/**
 * Interface that model the main Model of the application, which is used to design access to the domain of system usage.
 */
public interface UnderCutModel {

    /**
     * Method that returns the actual collection of values to be shown to the user.
     * 
     * @return 
     *          An InstantCollection ready to be shown to the user.
     */
    InstantCollection getValues();

    /**
     * Way to reset the model when application is stopped.
     * 
     * @throws IOException
     *          Exception due to a problem during model reset
     */
    void reset() throws IOException;

    /**
     * Method to add a GenericExceptionObeserver to application's model.
     * 
     * @param obs
     *          The observer to add
     * */
    void addExcObserver(GenericExceptionObserver obs);

    /**
     * Method to set what to log.
     *
     * @param tabsToLog
     *          List of the tabs that the user wants to be logged.
     *
     * @throws IOException
     *          If there was a problem during file's preparation
     */
    void setLogging(List<TabName> tabsToLog) throws IOException;

}
