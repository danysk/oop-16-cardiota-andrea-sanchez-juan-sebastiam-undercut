package org.undercut.view.utils;

import javafx.geometry.Rectangle2D;
import javafx.scene.Node;
import javafx.stage.Screen;

/**
 * Utility class to get the screen size values.
 */
public class ScreenUtilsImpl implements ScreenUtils {

    private final Rectangle2D bounds;

    /**
     * Constructor.
     */
    public ScreenUtilsImpl() {
        this.bounds = Screen.getPrimary().getVisualBounds();
    }

    @Override
    public double getScreenWidth() {
        return this.bounds.getWidth();
    }

    @Override
    public double getScreenHeight() {
        return this.bounds.getHeight();
    }

    @Override
    public void placeThisTo(final Node component, final double xPos, final double yPos) {
        component.setTranslateX((this.computeX(xPos)));
        component.setTranslateY((this.computeY(yPos)));
    }

    @Override
    public double computeX(final double value) {
        return (this.getScreenWidth() / 100) * value;
    }

    @Override
    public double computeY(final double value) {
        return (this.getScreenHeight() / 100) * value;
    }
}
