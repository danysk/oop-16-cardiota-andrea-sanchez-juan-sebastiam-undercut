package org.undercut.view.magicenums;

/**
 * Enumeration class to avoid magic numbers in the Section property building the
 * Gauges.
 */
public enum Sections {

    /**
     * RPM Gauge Section property values.
     */
    RPM_SECTION(11000, 12000, 13000, 13500, 14000),

    /**
     * RPM Gradient Section property values.
     */
    RPM_GRADIENT(2000, 4000, 6000, 8000, 10000),

    /**
     * Fuel pressure Gauge Section property values.
     */
    FUEL_P(3.5, 5, 0, 0, 0),

    /**
     * Airbox pressure Gauge Section property values.
     */
    AIR_BOX_P(0.85, 1, 0, 0, 0),

    /**
     * Time graphs Gauge Section property values.
     */
    TIME_GRAPHS(0, 0.5, 1, 0, 0),

    /**
     * Battery Gauge Section property values.
     */
    BATT(0, 0.5, 1, 0, 0);

    private final double firstField;
    private final double secondField;
    private final double thirdField;
    private final double fourthField;
    private final double fifthField;

    Sections(final double first, final double second, final double third, final double fourth,
            final double fifth) {
        this.firstField = first;
        this.secondField = second;
        this.thirdField = third;
        this.fourthField = fourth;
        this.fifthField = fifth;
    }

    /**
     * @return the first Section to create in the calling Gauge.
     */
    public double getFirst() {
        return this.firstField;
    }

    /**
     * @return the second Section to create in the calling Gauge.
     */
    public double getSecond() {
        return this.secondField;
    }

    /**
     * @return the third Section to create in the calling Gauge.
     */
    public double getThird() {
        return this.thirdField;
    }

    /**
     * @return the fourth Section to create in the calling Gauge.
     */
    public double getFourth() {
        return this.fourthField;
    }

    /**
     * @return the fifth Section to create in the calling Gauge.
     */
    public double getFifth() {
        return this.fifthField;
    }
}
