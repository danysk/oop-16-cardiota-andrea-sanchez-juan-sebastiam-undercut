package org.undercut.view.magicenums;

/**
 * Enumeration class to supply hexadicam expressed colors..
 */
public enum HEX {

    /**
     * Brakes temperature Gauges color property.
     **/
    BRKTMP("#B70000"),

    /**
     * Brakes pressure Gauges color property.
     **/
    BRKPRESSURE("#1A539E"),

    /**
     * Throttle usage Gauge color property.
     **/
    THROTTLE("#11B50B"),

    /**
     * Brakes usage Gauge color property.
     **/
    BRAKE("#E00202"),

    /**
     * Generated GForce (x-axis) Gauge color property.
     **/
    XG("#F60F0F"),

    /**
     * Generated GForce (y-axis) Gauge color property.
     **/
    YG("#281BE5"),

    /**
     * Generated GForce gyros Gauge color property.
     **/
    GYROS("#EA4326"),

    /**
     * Suspension travel Gauges color property.
     **/
    SUSPENSION("#FF7700");

    private final String colorCode;

    HEX(final String color) {
        this.colorCode = color;
    }

    /**
     * @return the color code in hexadecimal form.
     */
    public String getColor() {
        return this.colorCode;
    }
}
