package org.undercut.view;

import java.util.HashMap;
import java.util.Map;

import org.undercut.controller.UnderCutController;
import org.undercut.model.FieldName;
import org.undercut.view.utils.TabName;

import eu.hansolo.medusa.Gauge;
import eu.hansolo.tilesfx.Tile;
import javafx.application.Platform;
import javafx.scene.control.Control;
import javafx.scene.layout.Pane;

/**
 * Abstract class containing the template for every Tab that will be extending
 * this class.
 */
public abstract class LayoutSetUpImpl implements LayoutSetUp {

    private final TabName viewNameField;

    private final UnderCutController ucController;
    private final Map<FieldName, Control> gauges;

    /**
     * Constructor.
     * 
     * @param controller
     *            application main controller to set behaviours.
     * 
     * @param tabName
     *            The name of the actual view
     */
    public LayoutSetUpImpl(final UnderCutController controller, final TabName tabName) {
        this.ucController = controller;
        this.viewNameField = tabName;
        this.gauges = new HashMap<>();
    }

    /**
     * Constructor.
     * 
     * @param tabName
     *            The name of the actual view
     */
    public LayoutSetUpImpl(final TabName tabName) {
        this(null, tabName);
    }

    @Override
    public abstract Pane addGaugesToPane();

    @Override
    public void reset() {
        final Double zero = 0.0;
        final Map<FieldName, Number> tmp = new HashMap<>();
        this.gauges.entrySet().forEach(e -> tmp.put(e.getKey(), zero));
        this.update(tmp);
    }

    @Override
    public void update(final Map<FieldName, ? extends Number> values) {
        values.entrySet().forEach(e -> {
            final Control gauge = this.gauges.get(e.getKey());
            if (gauge instanceof Gauge) {
                Platform.runLater(() -> {
                    ((Gauge) gauge).setValue(e.getValue().doubleValue());
                });
            } else {
                Platform.runLater(() -> {
                    ((Tile) gauge).setValue(e.getValue().doubleValue());
                });
            }
        });
    }

    @Override
    public TabName viewName() {
        return this.viewNameField;
    }

    /**
     * @return the view controller
     */
    protected UnderCutController getUcController() {
        return this.ucController;
    }

    /**
     * @return Map that contains all the view gauges
     */
    protected Map<FieldName, Control> getGauges() {
        return this.gauges;
    }

}
