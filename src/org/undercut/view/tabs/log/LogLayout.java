package org.undercut.view.tabs.log;

import java.util.ArrayList;
import java.util.List;

import org.undercut.view.LayoutSetUpImpl;
import org.undercut.view.magicenums.UtilValues;
import org.undercut.view.utils.ScreenUtilsImpl;
import org.undercut.view.utils.TabName;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;

/**
 * Building class to draw the Log tab layout.
 */
public class LogLayout extends LayoutSetUpImpl {

    private static final String CLOSE_BUTTON_STYLE = "closeButton";
    private static final String BUTTON_STYLE = "logButton";
    private static final String LABEL_STYLE = "logLabel";
    private static final String CHECKBOX_STYLE = "checkbox";
    private static final String CONTACTS_STYLE = "contactsLabel";
    private static final String PATH_INFO_STYLE = "pathInfoLabel";

    private final List<CheckBox> checkOptions;
    private final List<TabName> tabSelected;

    /**
     * Class constructor of the ECULayout class.
     */
    public LogLayout() {
        super(TabName.LOG);
        this.checkOptions = new ArrayList<>();
        this.tabSelected = new ArrayList<>();
    }

    @Override
    public Pane addGaugesToPane() {
        final Pane pane = new Pane();
        final ScreenUtilsImpl util = new ScreenUtilsImpl();
        final Button set = new Button("Set");
        final Button cancel = new Button("Cancel");
        final Label dialog = new Label("Select the tab you want to log data from: (*)");
        final Label credits = new Label("UniBo Motorsport - University of Bologna Formula SAE Team");
        final Label contactsAC = new Label("Andrea Cardiota - andrea.cardiota@studio.unibo.it");
        final Label contactsJSS = new Label("Juan Sebastiam Sanchez - juan.sanchez2@studio.unibo.it");
        final Label pathInfo = new Label("*Log files will be stored in the \"UnderCut-Logs\"  folder located into the user home.");
        final CheckBox ecu = new CheckBox(TabName.ECU.toString());
        final CheckBox vcu = new CheckBox(TabName.VCU.toString());
        final CheckBox inertial = new CheckBox(TabName.INERTIAL.toString());
        final int optimalLogoFactor = 300;
        final double imgDimScaleFactor = util.getScreenWidth() / optimalLogoFactor;
        final double logoProportions = util.getScreenWidth() / imgDimScaleFactor;
        final ImageView uniBoMotorsportLogo = new ImageView(new Image(getClass().getResourceAsStream("/images/logo_UBM.png"), logoProportions, logoProportions, true, true));
        this.checkOptions.add(ecu);
        this.checkOptions.add(vcu);
        this.checkOptions.add(inertial);

        set.setDisable(true);
        set.setOnAction(e -> {
            set.setDisable(true);
            this.checkOptions.forEach(ck -> ck.setDisable(true));
            this.checkOptions.stream()
                             .filter(ck -> ck.isSelected())
                             .forEach(ck -> this.tabSelected.add(TabName.valueOf(ck.getText())));
        });
 
        cancel.setOnAction(e -> {
            set.setDisable(true);
            this.checkOptions.forEach(ck -> {
                ck.setDisable(false);
                ck.setSelected(false);
            });
            this.tabSelected.clear();
        });

        final EventHandler<ActionEvent> handler = e -> {
            if (e.getSource() instanceof CheckBox) {
                CheckBox chk = (CheckBox) e.getSource();
                if (TabName.ECU.toString().equals(chk.getText())) {
                    set.setDisable(false);
                } else if (TabName.VCU.toString().equals(chk.getText())) {
                    set.setDisable(false);
                } else if (TabName.INERTIAL.toString().equals(chk.getText())) {
                    set.setDisable(false);
                }
            }
        };

        cancel.getStyleClass().add(CLOSE_BUTTON_STYLE);
        set.getStyleClass().add(BUTTON_STYLE);
        dialog.getStyleClass().add(LABEL_STYLE);
        credits.getStyleClass().add(LABEL_STYLE);
        contactsAC.getStyleClass().add(CONTACTS_STYLE);
        contactsJSS.getStyleClass().add(CONTACTS_STYLE);
        pathInfo.getStyleClass().add(PATH_INFO_STYLE);

        this.checkOptions.forEach(ck -> {
            ck.setOnAction(handler);
            ck.getStyleClass().add(CHECKBOX_STYLE);
        });

        util.placeThisTo(dialog, UtilValues.LOGDIALOG.getX(), UtilValues.LOGDIALOG.getY());
        util.placeThisTo(ecu, UtilValues.LOGECU.getX(), UtilValues.LOGECU.getY());
        util.placeThisTo(vcu, UtilValues.LOGVCU.getX(), UtilValues.LOGVCU.getY());
        util.placeThisTo(inertial, UtilValues.LOGINERTIAL.getX(), UtilValues.LOGINERTIAL.getY());
        util.placeThisTo(cancel, UtilValues.LOGCANCEL.getX(), UtilValues.LOGCANCEL.getY());
        util.placeThisTo(set, UtilValues.LOGSET.getX(), UtilValues.LOGSET.getY());
        util.placeThisTo(uniBoMotorsportLogo, UtilValues.BIG_LOGO.getX(), UtilValues.BIG_LOGO.getY());
        util.placeThisTo(credits, UtilValues.CREDITS_LABEL.getX(), UtilValues.CREDITS_LABEL.getY());
        util.placeThisTo(contactsAC, UtilValues.AC_CONTACTS.getX(), UtilValues.AC_CONTACTS.getY());
        util.placeThisTo(contactsJSS, UtilValues.JSS_CONTACTS.getX(), UtilValues.JSS_CONTACTS.getY());
        util.placeThisTo(pathInfo, UtilValues.PATH_INFO.getX(), UtilValues.PATH_INFO.getY());


        pane.getChildren().addAll(uniBoMotorsportLogo, dialog, ecu, vcu, inertial, cancel, set,
                                  credits, contactsAC, contactsJSS, pathInfo);
        return pane;
    }

    /**
     * Method to get user's selection.
     * 
     * @return
     *          A list of the tabs selected by the user
     */
    public List<TabName> getTabsSelected() {
        return this.tabSelected;
    }

}
