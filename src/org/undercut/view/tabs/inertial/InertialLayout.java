package org.undercut.view.tabs.inertial;

import org.undercut.model.FieldName;
import org.undercut.view.LayoutSetUpImpl;
import org.undercut.view.magicenums.Angles;
import org.undercut.view.magicenums.HEX;
import org.undercut.view.magicenums.Unit;
import org.undercut.view.magicenums.UtilValues;
import org.undercut.view.utils.ScreenUtilsImpl;
import org.undercut.view.utils.TabName;

import eu.hansolo.medusa.Gauge;
import eu.hansolo.medusa.GaugeBuilder;
import eu.hansolo.medusa.TickMarkType;
import eu.hansolo.medusa.Gauge.NeedleShape;
import eu.hansolo.medusa.Gauge.NeedleSize;
import eu.hansolo.medusa.Gauge.NeedleType;
import eu.hansolo.medusa.Gauge.SkinType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;

/**
 * Building class to draw the Inertial tab layout.
 */
public class InertialLayout extends LayoutSetUpImpl {

    /**
     * Class constructor of the Intertial class.
     */
    public InertialLayout() {
        super(TabName.INERTIAL);
    }

    @Override
    public Pane addGaugesToPane() {
        final int fontSize = 30;
        final int decimalPrecision = 0;
        final Pane pane = new Pane();
        final ScreenUtilsImpl util = new ScreenUtilsImpl();
        final int optimalLogoFactor = 100;
        final double imgDimScaleFactor = util.getScreenWidth() / optimalLogoFactor;
        final double logoProportions = util.getScreenWidth() / imgDimScaleFactor;
        final ImageView uniBoMotorsportLogo = new ImageView(new Image(getClass().getResourceAsStream("/images/logo_UBM.png"), logoProportions, logoProportions, true, true));

        final Gauge gyroX = GaugeBuilder.create()
                                        .maxSize(util.getScreenWidth() / UtilValues.GYROX.getWidthFactor(), 
                                                 util.getScreenHeight() / UtilValues.GYROX.getHeightFactor())
                                        .startAngle(Angles.INERTIAL.getStartingAngle())
                                        .angleRange(Angles.INERTIAL.getAngleRange())
                                        .minValue(UtilValues.GYROX.getMin())
                                        .maxValue(UtilValues.GYROX.getMax())
                                        .minorTickMarksVisible(false)
                                        .majorTickMarkType(TickMarkType.BOX)
                                        .mediumTickMarkType(TickMarkType.BOX)
                                        .title("Gyro X")
                                        .unit(Unit.GYROS.getUnit())
                                        .needleType(NeedleType.AVIONIC)
                                        .needleShape(NeedleShape.FLAT)
                                        .needleSize(NeedleSize.STANDARD)
                                        .needleColor(Color.web(HEX.GYROS.getColor()))
                                        .knobColor(Gauge.DARK_COLOR)
                                        .decimals(decimalPrecision)
                                        .customTickLabelsEnabled(true)
                                        .customTickLabelFontSize(fontSize)
                                        .customTickLabels("-100", "", "-50", "", "0", "", "50", "", "100")
                                        .build();
        super.getGauges().put(FieldName.GYRO_X, gyroX);

        final Gauge gyroY = GaugeBuilder.create()
                                        .maxSize(util.getScreenWidth() / UtilValues.GYROY.getWidthFactor(), 
                                                 util.getScreenHeight() / UtilValues.GYROY.getHeightFactor()) 
                                        .startAngle(Angles.INERTIAL.getStartingAngle())
                                        .angleRange(Angles.INERTIAL.getAngleRange())
                                        .minValue(UtilValues.GYROY.getMin())
                                        .maxValue(UtilValues.GYROY.getMax())
                                        .minorTickMarksVisible(false)
                                        .majorTickMarkType(TickMarkType.BOX)
                                        .mediumTickMarkType(TickMarkType.BOX)
                                        .title("Gyro Y")
                                        .unit(Unit.GYROS.getUnit())
                                        .needleType(NeedleType.AVIONIC)
                                        .needleShape(NeedleShape.FLAT)
                                        .needleSize(NeedleSize.STANDARD)
                                        .needleColor(Color.web(HEX.GYROS.getColor()))
                                        .knobColor(Gauge.DARK_COLOR)
                                        .decimals(decimalPrecision)
                                        .customTickLabelsEnabled(true)
                                        .customTickLabelFontSize(fontSize)
                                        .customTickLabels("-100", "", "-50", "", "0", "", "50", "", "100")
                                        .build();
        super.getGauges().put(FieldName.GYRO_Y, gyroY);

        final Gauge gyroZ = GaugeBuilder.create()
                                        .maxSize(util.getScreenWidth() / UtilValues.GYROZ.getWidthFactor(), 
                                                 util.getScreenHeight() / UtilValues.GYROX.getHeightFactor())
                                        .startAngle(Angles.INERTIAL.getStartingAngle())
                                        .angleRange(Angles.INERTIAL.getAngleRange())
                                        .minValue(UtilValues.GYROZ.getMin())
                                        .maxValue(UtilValues.GYROZ.getMax())
                                        .minorTickMarksVisible(false)
                                        .majorTickMarkType(TickMarkType.BOX)
                                        .mediumTickMarkType(TickMarkType.BOX)
                                        .title("Gyro Z")
                                        .unit(Unit.GYROS.getUnit())
                                        .needleType(NeedleType.AVIONIC)
                                        .needleShape(NeedleShape.FLAT)
                                        .needleSize(NeedleSize.STANDARD)
                                        .needleColor(Color.web(HEX.GYROS.getColor()))
                                        .knobColor(Gauge.DARK_COLOR)
                                        .decimals(decimalPrecision)
                                        .customTickLabelsEnabled(true)
                                        .customTickLabelFontSize(fontSize)
                                        .customTickLabels("-100", "", "-50", "", "0", "", "50", "", "100")
                                        .build();
        super.getGauges().put(FieldName.GYRO_Z, gyroZ);

        final Gauge xGForce = GaugeBuilder.create()
                                           .maxSize(util.getScreenWidth() / UtilValues.GX.getWidthFactor(), 
                                                    util.getScreenHeight() / UtilValues.GX.getHeightFactor())
                                           .skinType(SkinType.SPACE_X)
                                           .startAngle(Angles.INERTIAL.getStartingAngle())
                                           .angleRange(Angles.INERTIAL.getAngleRange())
                                           .minValue(UtilValues.GX.getMin())
                                           .maxValue(UtilValues.GX.getMax())
                                           .title("X axis G-Force")
                                           .unit(Unit.GFORCE.getUnit())
                                           .titleColor(Color.BLACK)
                                           .unitColor(Color.BLACK)
                                           .barColor(Color.web(HEX.XG.getColor()))
                                           .valueColor(Color.BLACK)
                                           .build();
        super.getGauges().put(FieldName.ACC_X, xGForce);

        final Gauge yGForce = GaugeBuilder.create()
                                .maxSize(util.getScreenWidth() / UtilValues.GY.getWidthFactor(), 
                                         util.getScreenHeight() / UtilValues.GY.getHeightFactor())
                                .skinType(SkinType.SPACE_X)
                                .startAngle(Angles.INERTIAL.getStartingAngle())
                                .angleRange(Angles.INERTIAL.getAngleRange())
                                .minValue(UtilValues.GY.getMin())
                                .maxValue(UtilValues.GY.getMax())
                                .title("Y axis G-Force")
                                .unit(Unit.GFORCE.getUnit())
                                .titleColor(Color.BLACK)
                                .unitColor(Color.BLACK)
                                .barColor(Color.web(HEX.YG.getColor()))
                                .valueColor(Color.BLACK).build();
        super.getGauges().put(FieldName.ACC_Y, yGForce);

        final Gauge zGForce = GaugeBuilder.create()
                                    .maxSize(util.getScreenWidth() / UtilValues.GZ.getWidthFactor(), 
                                             util.getScreenHeight() / UtilValues.GZ.getHeightFactor())
                                    .skinType(SkinType.SPACE_X)
                                    .startAngle(Angles.INERTIAL.getStartingAngle())
                                    .angleRange(Angles.INERTIAL.getAngleRange())
                                    .minValue(UtilValues.GZ.getMin())
                                    .maxValue(UtilValues.GZ.getMax())
                                    .title("Z axis G-Force")
                                    .unit(Unit.GFORCE.getUnit())
                                    .titleColor(Color.BLACK)
                                    .unitColor(Color.BLACK)
                                    .barColor(Color.GREEN)
                                    .valueColor(Color.BLACK)
                                    .build();
        super.getGauges().put(FieldName.ACC_Z, zGForce);

        util.placeThisTo(gyroX, UtilValues.GYROX.getX(), UtilValues.GYROX.getY());
        util.placeThisTo(gyroY, UtilValues.GYROY.getX(), UtilValues.GYROY.getY());
        util.placeThisTo(gyroZ, UtilValues.GYROZ.getX(), UtilValues.GYROZ.getY());
        util.placeThisTo(xGForce, UtilValues.GX.getX(), UtilValues.GX.getY());
        util.placeThisTo(yGForce, UtilValues.GY.getX(), UtilValues.GY.getY());
        util.placeThisTo(zGForce, UtilValues.GZ.getX(), UtilValues.GZ.getY());
        util.placeThisTo(uniBoMotorsportLogo, UtilValues.LOGO.getX(), UtilValues.LOGO.getY());
        pane.getChildren().addAll(gyroX, gyroY, gyroZ, xGForce, yGForce, zGForce, uniBoMotorsportLogo);

        return pane;
    }

}
