package org.undercut.view.tabs.vcu;

import org.undercut.model.FieldName;
import org.undercut.view.LayoutSetUpImpl;
import org.undercut.view.magicenums.HEX;
import org.undercut.view.magicenums.Sections;
import org.undercut.view.magicenums.Unit;
import org.undercut.view.magicenums.UtilValues;
import org.undercut.view.utils.ScreenUtilsImpl;
import org.undercut.view.utils.TabName;

import eu.hansolo.medusa.Gauge;
import eu.hansolo.medusa.GaugeBuilder;
import eu.hansolo.medusa.LcdDesign;
import eu.hansolo.medusa.Gauge.SkinType;
import eu.hansolo.tilesfx.Tile;
import eu.hansolo.tilesfx.TileBuilder;
import javafx.geometry.Orientation;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.paint.Stop;

/**
 * Building class to draw the VCU tab layout.
 */
public class VCULayout extends LayoutSetUpImpl {

    private static final int THRESHOLD = 50;

    /**
     * Main constructor of the VCULayout class.
     */
    public VCULayout() {
        super(TabName.ECU);
    }

    @Override
    public Pane addGaugesToPane() {
        final Pane pane = new Pane();
        final ScreenUtilsImpl util = new ScreenUtilsImpl();
        final int optimalLogoFactor = 100;
        final double imgDimScaleFactor = util.getScreenWidth() / optimalLogoFactor;
        final double logoProportions = util.getScreenWidth() / imgDimScaleFactor;
        final ImageView uniBoMotorsportLogo = new ImageView(new Image(getClass().getResourceAsStream("/images/logo_UBM.png"), logoProportions, logoProportions, true, true));

        final Gauge flBrakeTmp = GaugeBuilder.create()
                                             .maxSize(util.getScreenWidth() / UtilValues.FLBRKTMP.getWidthFactor(), 
                                                      util.getScreenHeight() / UtilValues.FLBRKTMP.getHeightFactor())
                                             .skinType(SkinType.LINEAR)
                                             .orientation(Orientation.VERTICAL)
                                             .lcdVisible(true)
                                             .lcdDesign(LcdDesign.GRAY_PURPLE)
                                             .barColor(Color.web(HEX.BRKTMP.getColor()))
                                             .minValue(UtilValues.FLBRKTMP.getMin())
                                             .maxValue(UtilValues.FLBRKTMP.getMax())
                                             .title("FL brake")
                                             .unit(Unit.TMP.getUnit())
                                             .build();
        super.getGauges().put(FieldName.FL_BRK_T, flBrakeTmp);

        final Gauge frBrakeTmp = GaugeBuilder.create()
                                             .maxSize(util.getScreenWidth() / UtilValues.FRBRKTMP.getWidthFactor(), 
                                                      util.getScreenHeight() / UtilValues.FRBRKTMP.getHeightFactor())
                                             .skinType(SkinType.LINEAR)
                                             .orientation(Orientation.VERTICAL)
                                             .lcdVisible(true)
                                             .lcdDesign(LcdDesign.GRAY_PURPLE)
                                             .barColor(Color.web(HEX.BRKTMP.getColor()))
                                             .minValue(UtilValues.FRBRKTMP.getMin())
                                             .maxValue(UtilValues.FRBRKTMP.getMax())
                                             .title("FR brake")
                                             .unit(Unit.TMP.getUnit())
                                             .build();
        super.getGauges().put(FieldName.FR_BRK_T, frBrakeTmp);

        final Gauge rlBrakeTmp = GaugeBuilder.create()
                                             .maxSize(util.getScreenWidth() / UtilValues.RLBRKTMP.getWidthFactor(), 
                                                      util.getScreenHeight() / UtilValues.RLBRKTMP.getHeightFactor())
                                             .skinType(SkinType.LINEAR)
                                             .orientation(Orientation.VERTICAL)
                                             .lcdVisible(true)
                                             .lcdDesign(LcdDesign.GRAY_PURPLE)
                                             .barColor(Color.web(HEX.BRKTMP.getColor()))
                                             .minValue(UtilValues.RLBRKTMP.getMin())
                                             .maxValue(UtilValues.RLBRKTMP.getMax())
                                             .title("RL brake")
                                             .unit(Unit.TMP.getUnit())
                                             .build();
        super.getGauges().put(FieldName.RL_BRK_T, rlBrakeTmp);

        final Gauge rrBrakeTmp = GaugeBuilder.create()
                                             .maxSize(util.getScreenWidth() / UtilValues.RRBRKTMP.getWidthFactor(), 
                                                      util.getScreenHeight() / UtilValues.RRBRKTMP.getHeightFactor())
                                             .skinType(SkinType.LINEAR)
                                             .orientation(Orientation.VERTICAL)
                                             .lcdVisible(true)
                                             .lcdDesign(LcdDesign.GRAY_PURPLE)
                                             .barColor(Color.web(HEX.BRKTMP.getColor()))
                                             .minValue(UtilValues.RRBRKTMP.getMin())
                                             .maxValue(UtilValues.RRBRKTMP.getMax())
                                             .title("RR brake")
                                             .unit(Unit.SUSP.getUnit())
                                             .build();
        super.getGauges().put(FieldName.RR_BRK_T, rrBrakeTmp);

        final Gauge flSuspCompr = GaugeBuilder.create()
                                              .maxSize(util.getScreenWidth() / UtilValues.FLSUSP.getWidthFactor(), 
                                                       util.getScreenHeight() / UtilValues.FLSUSP.getHeightFactor())
                                              .skinType(SkinType.LINEAR)
                                              .orientation(Orientation.VERTICAL)
                                              .lcdVisible(true)
                                              .lcdDesign(LcdDesign.GRAY_PURPLE)
                                              .barColor(Color.web(HEX.SUSPENSION.getColor()))
                                              .minValue(UtilValues.FLSUSP.getMin())
                                              .maxValue(UtilValues.FLSUSP.getMax())
                                              .title("FL suspension")
                                              .unit(Unit.SUSP.getUnit())
                                              .build();
        super.getGauges().put(FieldName.FL_SUS_TL, flSuspCompr);

        final Gauge frSuspCompr = GaugeBuilder.create()
                                              .maxSize(util.getScreenWidth() / UtilValues.FRSUSP.getWidthFactor(), 
                                                       util.getScreenHeight() / UtilValues.FRSUSP.getHeightFactor())
                                              .skinType(SkinType.LINEAR)
                                              .orientation(Orientation.VERTICAL)
                                              .lcdVisible(true)
                                              .lcdDesign(LcdDesign.GRAY_PURPLE)
                                              .barColor(Color.web(HEX.SUSPENSION.getColor()))
                                              .minValue(UtilValues.FRSUSP.getMin())
                                              .maxValue(UtilValues.FRSUSP.getMax())
                                              .title("FR suspension")
                                              .unit(Unit.SUSP.getUnit())
                                              .build();
        super.getGauges().put(FieldName.FR_SUS_TL, frSuspCompr);

        final Gauge rlSuspCompr = GaugeBuilder.create()
                                              .maxSize(util.getScreenWidth() / UtilValues.RLSUSP.getWidthFactor(), 
                                                       util.getScreenHeight() / UtilValues.RLSUSP.getHeightFactor())
                                              .skinType(SkinType.LINEAR)
                                              .orientation(Orientation.VERTICAL)
                                              .lcdVisible(true)
                                              .lcdDesign(LcdDesign.GRAY_PURPLE)
                                              .barColor(Color.web(HEX.SUSPENSION.getColor()))
                                              .minValue(UtilValues.RLSUSP.getMin())
                                              .maxValue(UtilValues.RLSUSP.getMax())
                                              .title("RL suspension")
                                              .unit(Unit.SUSP.getUnit())
                                              .build();
        super.getGauges().put(FieldName.RL_SUS_TL, rlSuspCompr);

        final Gauge rrSuspCompr = GaugeBuilder.create()
                                              .maxSize(util.getScreenWidth() / UtilValues.RRSUSP.getWidthFactor(), 
                                                       util.getScreenHeight() / UtilValues.RRSUSP.getHeightFactor())
                                              .skinType(SkinType.LINEAR)
                                              .orientation(Orientation.VERTICAL)
                                              .lcdVisible(true)
                                              .lcdDesign(LcdDesign.GRAY_PURPLE)
                                              .barColor(Color.web(HEX.SUSPENSION.getColor()))
                                              .minValue(UtilValues.RRSUSP.getMin())
                                              .maxValue(UtilValues.RRSUSP.getMax())
                                              .title("RR suspension")
                                              .unit(Unit.SUSP.getUnit())
                                              .build();
        super.getGauges().put(FieldName.RR_SUS_TL, rrSuspCompr);

        final Tile batteryVoltage = TileBuilder.create()
                                               .prefSize(util.getScreenWidth() / UtilValues.BVOLT.getWidthFactor(), 
                                                         util.getScreenHeight() / UtilValues.BVOLT.getHeightFactor())
                                               .skinType(Tile.SkinType.SPARK_LINE)
                                               .minValue(UtilValues.BVOLT.getMin())
                                               .maxValue(UtilValues.BVOLT.getMax())
                                               .title("Battery voltage")
                                               .unit(Unit.VOLT.getUnit())
                                               .gradientStops(new Stop(Sections.BATT.getFirst(), Tile.GREEN),
                                                              new Stop(Sections.BATT.getSecond(), Tile.YELLOW),
                                                              new Stop(Sections.BATT.getThird(), Tile.RED))
                                               .strokeWithGradient(true)
                                               .build();
        super.getGauges().put(FieldName.BTR_V, batteryVoltage);

        final Tile batteryCurrent = TileBuilder.create()
                                               .prefSize(util.getScreenWidth() / UtilValues.BCURR.getWidthFactor(), 
                                                         util.getScreenHeight() / UtilValues.BCURR.getHeightFactor())
                                               .skinType(Tile.SkinType.SPARK_LINE)
                                               .minValue(UtilValues.BCURR.getMin())
                                               .maxValue(UtilValues.BCURR.getMax())
                                               .title("Battery current")
                                               .unit(Unit.AMP.getUnit())
                                               .gradientStops(new Stop(Sections.BATT.getFirst(), Tile.GREEN),
                                                              new Stop(Sections.BATT.getSecond(), Tile.YELLOW),
                                                              new Stop(Sections.BATT.getThird(), Tile.RED))
                                               .strokeWithGradient(true)
                                               .build();
        super.getGauges().put(FieldName.BTR_C, batteryCurrent);

        final Tile currentRegulator = TileBuilder.create()
                                                 .prefSize(util.getScreenWidth() / UtilValues.REGCURR.getWidthFactor(), 
                                                           util.getScreenHeight() / UtilValues.REGCURR.getHeightFactor())
                                                 .skinType(Tile.SkinType.SPARK_LINE)
                                                 .minValue(UtilValues.REGCURR.getMin())
                                                 .maxValue(UtilValues.REGCURR.getMax())
                                                 .title("Current regulator")
                                                 .unit(Unit.AMP.getUnit())
                                                 .gradientStops(new Stop(Sections.BATT.getFirst(), Tile.GREEN),
                                                                new Stop(Sections.BATT.getSecond(), Tile.YELLOW),
                                                                new Stop(Sections.BATT.getThird(), Tile.RED))
                                                 .strokeWithGradient(true)
                                                 .build();
        super.getGauges().put(FieldName.C_REG, currentRegulator);

        final Gauge frontBrakesPressure = GaugeBuilder.create()
                                                      .maxSize(util.getScreenWidth() / UtilValues.FRONTBRKP.getWidthFactor(), 
                                                               util.getScreenHeight() / UtilValues.FRONTBRKP.getHeightFactor())
                                                      .skinType(SkinType.LINEAR)
                                                      .orientation(Orientation.VERTICAL)
                                                      .lcdVisible(true)
                                                      .lcdDesign(LcdDesign.GRAY_PURPLE)
                                                      .barColor(Color.web(HEX.BRKPRESSURE.getColor()))
                                                      .minValue(UtilValues.FRONTBRKP.getMin())
                                                      .maxValue(UtilValues.FRONTBRKP.getMax())
                                                      .title("Front brakes")
                                                      .unit(Unit.PRESSURE.getUnit())
                                                      .build();
        super.getGauges().put(FieldName.F_BRK_P, frontBrakesPressure);

        final Gauge rearBrakesPressure = GaugeBuilder.create()
                                                     .maxSize(util.getScreenWidth() / UtilValues.REARBRKP.getWidthFactor(), 
                                                              util.getScreenHeight() / UtilValues.REARBRKP.getHeightFactor())
                                                     .skinType(SkinType.LINEAR)
                                                     .orientation(Orientation.VERTICAL)
                                                     .lcdVisible(true)
                                                     .lcdDesign(LcdDesign.GRAY_PURPLE)
                                                     .barColor(Color.web(HEX.BRKPRESSURE.getColor()))
                                                     .minValue(UtilValues.REARBRKP.getMin())
                                                     .maxValue(UtilValues.REARBRKP.getMax())
                                                     .title("Rear brakes")
                                                     .unit(Unit.PRESSURE.getUnit())
                                                     .build();
        super.getGauges().put(FieldName.R_BRK_P, rearBrakesPressure);

        final Gauge flWheelSpeed = GaugeBuilder.create()
                                               .maxSize(util.getScreenWidth() / UtilValues.FLWHEEL.getWidthFactor(), 
                                                        util.getScreenHeight() / UtilValues.FLWHEEL.getHeightFactor())
                                               .skinType(SkinType.LCD)
                                               .title("FL wheel")
                                               .oldValueVisible(true)
                                               .lcdDesign(LcdDesign.BLUE_LIGHTBLUE2)
                                               .unit(Unit.SPEED.getUnit())
                                               .build();
        super.getGauges().put(FieldName.FL_WH_SP, flWheelSpeed);

        final Gauge frWheelSpeed = GaugeBuilder.create()
                                               .maxSize(util.getScreenWidth() / UtilValues.FRWHEEL.getWidthFactor(), 
                                                        util.getScreenHeight() / UtilValues.FRWHEEL.getHeightFactor())
                                               .skinType(SkinType.LCD)
                                               .title("FR wheel")
                                               .oldValueVisible(true)
                                               .lcdDesign(LcdDesign.BLUE_LIGHTBLUE2)
                                               .unit(Unit.SPEED.getUnit())
                                               .build();
        super.getGauges().put(FieldName.FR_WH_SP, frWheelSpeed);

        final Gauge rlWheelSpeed = GaugeBuilder.create()
                                               .maxSize(util.getScreenWidth() / UtilValues.RLWHEEL.getWidthFactor(), 
                                                        util.getScreenHeight() / UtilValues.RLWHEEL.getHeightFactor())
                                               .skinType(SkinType.LCD)
                                               .title("RL wheel")
                                               .oldValueVisible(true)
                                               .lcdDesign(LcdDesign.BLUE_LIGHTBLUE2)
                                               .unit(Unit.SPEED.getUnit())
                                               .build();
        super.getGauges().put(FieldName.RL_WH_SP, rlWheelSpeed);

        final Gauge rrWheelSpeed = GaugeBuilder.create()
                                               .maxSize(util.getScreenWidth() / UtilValues.RRWHEEL.getWidthFactor(), 
                                                        util.getScreenHeight() / UtilValues.RRWHEEL.getHeightFactor())
                                               .skinType(SkinType.LCD)
                                               .title("RR wheel")
                                               .oldValueVisible(true)
                                               .lcdDesign(LcdDesign.BLUE_LIGHTBLUE2)
                                               .unit(Unit.SPEED.getUnit())
                                               .build();
        super.getGauges().put(FieldName.RR_WH_SP, rrWheelSpeed);

        final Gauge brakeBias = GaugeBuilder.create()
                                            .skinType(SkinType.BAR)
                                            .maxSize(util.getScreenWidth() / UtilValues.BRKBIAS.getWidthFactor(), 
                                                     util.getScreenHeight() / UtilValues.BRKBIAS.getHeightFactor())
                                            .title("Brake bias")
                                            .unit(Unit.PERC.getUnit())
                                            .barColor(Color.GREEN)
                                            .maxValue(UtilValues.BRKBIAS.getMax())
                                            .threshold(VCULayout.THRESHOLD)
                                            .build();
        super.getGauges().put(FieldName.BRK_B, brakeBias);

        final Gauge steeringAngle = GaugeBuilder.create()
                                                .skinType(SkinType.BAR)
                                                .maxSize(util.getScreenWidth() / UtilValues.STRANGLE.getWidthFactor(), 
                                                         util.getScreenHeight() / UtilValues.STRANGLE.getHeightFactor())
                                                .title("Steering angle")
                                                .unit(Unit.ANGLE.getUnit())
                                                .barColor(Color.BLUE)
                                                .maxValue(UtilValues.STRANGLE.getMax())
                                                .threshold(VCULayout.THRESHOLD)
                                                .build();
        super.getGauges().put(FieldName.STR_ANG, steeringAngle);

        final Gauge slipLeft = GaugeBuilder.create()
                                           .maxSize(util.getScreenWidth() / UtilValues.SLIPL.getWidthFactor(), 
                                                    util.getScreenHeight() / UtilValues.SLIPL.getHeightFactor())
                                           .skinType(SkinType.LCD)
                                           .title("Slip left")
                                           .oldValueVisible(true)
                                           .lcdDesign(LcdDesign.BLACK)
                                           .build();
        super.getGauges().put(FieldName.SLIP_L, slipLeft);

        final Gauge slipRight = GaugeBuilder.create()
                                            .maxSize(util.getScreenWidth() / UtilValues.SLIPR.getWidthFactor(), 
                                                     util.getScreenHeight() / UtilValues.SLIPR.getHeightFactor())
                                            .skinType(SkinType.LCD)
                                            .title("Slip right")
                                            .oldValueVisible(true)
                                            .lcdDesign(LcdDesign.BLACK)
                                            .build();
        super.getGauges().put(FieldName.SLIP_R, slipRight);

        final Gauge chassisTemp = GaugeBuilder.create()
                                              .maxSize(util.getScreenWidth() / UtilValues.CHSSTMP.getWidthFactor(), 
                                                       util.getScreenHeight() / UtilValues.CHSSTMP.getHeightFactor())
                                              .skinType(SkinType.LCD)
                                              .title("Chassis temp")
                                              .oldValueVisible(true)
                                              .lcdDesign(LcdDesign.STANDARD_GREEN)
                                              .unit(Unit.TMP.getUnit())
                                              .build();
        super.getGauges().put(FieldName.CHSS_T, chassisTemp);

        final Gauge engineTmp = GaugeBuilder.create()
                                            .maxSize(util.getScreenWidth() / UtilValues.CHSSTMP.getWidthFactor(), 
                                                     util.getScreenHeight() / UtilValues.CHSSTMP.getHeightFactor())
                                            .skinType(SkinType.LCD)
                                            .title("Engine temp")
                                            .oldValueVisible(true)
                                            .lcdDesign(LcdDesign.STANDARD_GREEN)
                                            .unit(Unit.TMP.getUnit())
                                            .build();
        super.getGauges().put(FieldName.ENG_T, engineTmp);

        util.placeThisTo(flWheelSpeed, UtilValues.FLWHEEL.getX(), UtilValues.FLWHEEL.getY());
        util.placeThisTo(frWheelSpeed, UtilValues.FRWHEEL.getX(), UtilValues.FRWHEEL.getY());
        util.placeThisTo(rlWheelSpeed, UtilValues.RLWHEEL.getX(), UtilValues.RLWHEEL.getY());
        util.placeThisTo(rrWheelSpeed, UtilValues.RRWHEEL.getX(), UtilValues.RRWHEEL.getY());
        util.placeThisTo(frontBrakesPressure, UtilValues.FRONTBRKP.getX(), UtilValues.FRONTBRKP.getY());
        util.placeThisTo(rearBrakesPressure, UtilValues.REARBRKP.getX(), UtilValues.REARBRKP.getY());
        util.placeThisTo(flBrakeTmp, UtilValues.FLBRKTMP.getX(), UtilValues.FLBRKTMP.getY());
        util.placeThisTo(frBrakeTmp, UtilValues.FRBRKTMP.getX(), UtilValues.FRBRKTMP.getY());
        util.placeThisTo(rlBrakeTmp, UtilValues.RLBRKTMP.getX(), UtilValues.RLBRKTMP.getY());
        util.placeThisTo(rrBrakeTmp, UtilValues.RRBRKTMP.getX(), UtilValues.RRBRKTMP.getY());
        util.placeThisTo(flSuspCompr, UtilValues.FLSUSP.getX(), UtilValues.FLSUSP.getY());
        util.placeThisTo(frSuspCompr, UtilValues.FRSUSP.getX(), UtilValues.FRSUSP.getY());
        util.placeThisTo(rlSuspCompr, UtilValues.RLSUSP.getX(), UtilValues.RLSUSP.getY());
        util.placeThisTo(rrSuspCompr, UtilValues.RRSUSP.getX(), UtilValues.RRSUSP.getY());
        util.placeThisTo(brakeBias, UtilValues.BRKBIAS.getX(), UtilValues.BRKBIAS.getY());
        util.placeThisTo(steeringAngle, UtilValues.STRANGLE.getX(), UtilValues.STRANGLE.getY());
        util.placeThisTo(slipLeft, UtilValues.SLIPL.getX(), UtilValues.SLIPL.getY());
        util.placeThisTo(slipRight, UtilValues.SLIPR.getX(), UtilValues.SLIPR.getY());
        util.placeThisTo(chassisTemp, UtilValues.CHSSTMP.getX(), UtilValues.CHSSTMP.getY());
        util.placeThisTo(engineTmp, UtilValues.ENGINETMP.getX(), UtilValues.ENGINETMP.getY());
        util.placeThisTo(batteryVoltage, UtilValues.BVOLT.getX(), UtilValues.BVOLT.getY());
        util.placeThisTo(batteryCurrent, UtilValues.BCURR.getX(), UtilValues.BCURR.getY());
        util.placeThisTo(currentRegulator, UtilValues.REGCURR.getX(), UtilValues.REGCURR.getY());
        util.placeThisTo(uniBoMotorsportLogo, UtilValues.LOGO.getX(), UtilValues.LOGO.getY());

        pane.getChildren().addAll(flWheelSpeed, 
                                  frWheelSpeed, 
                                  frontBrakesPressure, 
                                  flBrakeTmp, 
                                  frBrakeTmp, 
                                  flSuspCompr, 
                                  frSuspCompr, 
                                  rearBrakesPressure, 
                                  rlBrakeTmp, 
                                  rrBrakeTmp, 
                                  rlSuspCompr, 
                                  rrSuspCompr, 
                                  rlWheelSpeed, 
                                  rrWheelSpeed, 
                                  brakeBias, 
                                  steeringAngle, 
                                  slipLeft,
                                  slipRight, 
                                  chassisTemp, 
                                  engineTmp, 
                                  batteryVoltage, 
                                  batteryCurrent, 
                                  currentRegulator,
                                  uniBoMotorsportLogo);
        return pane;
    }

}
