package org.undercut.view;

import java.util.Map;

import org.undercut.model.FieldName;
import org.undercut.view.utils.TabName;

import javafx.scene.layout.Pane;

/**
 * Interface for the main View class.
 */
public interface LayoutSetUp {

    /**
     * Adds all the needed gauges to the Pane.
     * 
     * @return The whole Pane.
     */
    Pane addGaugesToPane();

    /**
     * Stops the simulation.
     */
    void reset();

    /**
     * Sets a new value to a Gauge.
     * 
     * @param value
     *            the value of the counter
     */
    void update(Map<FieldName, ? extends Number> value);

    /**
     * @return the view name
     */
    TabName viewName();
}
