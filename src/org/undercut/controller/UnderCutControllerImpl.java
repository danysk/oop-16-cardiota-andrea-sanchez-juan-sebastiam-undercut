package org.undercut.controller;

import java.io.IOException;

import org.undercut.model.UnderCutModel;
import org.undercut.model.observers.GenericExceptionObserver;
import org.undercut.view.UnderCutView;

import javafx.application.Platform;

/**
 * Main controller class.
 */
public class UnderCutControllerImpl implements UnderCutController {

    private static final String RESTART_COMMAND = "java -jar UnderCut.jar";
    private static final Integer REFRESH_DELTA = 50;

    private boolean isLoggingEnable;

    private final UnderCutModel ucModel;
    private UnderCutView ucView;
    private Refresher refresher;

    /**
     * Class constructor of the UnderCutControllerImpl class.
     *
     * @param model
     *          Model object to follow
     */
    public UnderCutControllerImpl(final UnderCutModel model) {
        this.isLoggingEnable = Boolean.TRUE;
        this.ucModel = model;
        this.ucModel.addExcObserver(this.setDefaultExceptionObserver());
    }

    @Override
    public synchronized void startUC() {
        this.checkAndDisableLogging();
        if (this.refresher == null) {
            this.refresher = new Refresher();
            this.refresher.start();
        } else {
            this.showExceptionDialog("IllegalStateException", "Refresher is already started");
        }
    }

    @Override
    public synchronized void stopUC() {
        this.checkAndEnableLogging();
        if (this.refresher == null) {
            this.showExceptionDialog("IllegalStateException", "Refresher is already stopped");
        }
        this.refresher.stopRefresh();
        try {
            this.refresher.join();
            this.ucView.reset();
            this.ucModel.reset();
        } catch (InterruptedException e) {
            this.showExceptionDialog("InterruptedException", "Occured stopping the application");
        } catch (IOException e) {
            this.showExceptionDialog("IOException", "Occured during model reset");
        }
        this.refresher = null;
    }

    @Override
    public void registerView(final UnderCutView view) {
        this.ucView = view;
    }

    /**
     * Method to operate the graphical refresh.
     */
    private void updateView() {
        this.ucView.update(this.ucModel.getValues());
    }

    /**
     * Method to ask view to launch a dialog due to an exception.
     * 
     * @param exceptionName
     *          The name of the exception that is occurred
     * 
     * @param message
     *          The message to show to the user
     */
    private void showExceptionDialog(final String exceptionName, final String message) {
        Platform.runLater(() -> {
            this.ucView.launchExceptionDialog(exceptionName + ": " + message, (e) -> this.restart());
        });
    }

    /**
     * Method to force application restart due to an exception.
     */
    private void restart() {
        try {
            Runtime.getRuntime().exec(UnderCutControllerImpl.RESTART_COMMAND);
            Runtime.getRuntime().exit(1);
        } catch (IOException e) {
            this.showExceptionDialog("IOException", "Occured during application restar (jar not found)");
        }
    }

    /**
     * Method to check if log tab is enabled. If it's not, it enables it and asks the view to show the changes.
     */
    private void checkAndEnableLogging() {
        if (this.isLoggingEnable == Boolean.FALSE) {
            this.isLoggingEnable = Boolean.TRUE;
            this.ucView.enableLogging();
        }
    }

    /**
     * Method to check if log tab is disabled. If it's not the case, disabling it and asks the view to show the change.
     */
    private void checkAndDisableLogging() {
        if (this.isLoggingEnable == Boolean.TRUE) {
            this.isLoggingEnable = Boolean.FALSE;
            try {
                this.ucModel.setLogging(this.ucView.getSelectedToLog());
            } catch (IllegalArgumentException e) {
                showExceptionDialog("IllegalArgumentException", "Error in logging setup");
            } catch (IOException e) {
                showExceptionDialog("IOException", "Error on log's run");
            }
            this.ucView.disableLogging();
        }
    }

    /**
     * Method that return the bahavior of an delfault exception observer.
     *
     * @return
     *          The behavior of the observer
     */
    private GenericExceptionObserver setDefaultExceptionObserver() {
        return (a, ex, exMessage) -> {
            synchronized (this) { 
                this.refresher.stopRefresh();
            }
            try {
                this.ucView.reset();
                synchronized (this) { 
                    this.refresher.join();
                }
                throw ex;
            } catch (Exception e) {
                this.showExceptionDialog(ex.getClass().getSimpleName().toString(), exMessage);
            }
        };
    }

    /**
     * Class defining objects whose job is to keep animation alive, by requiring graphic updates
     * every X milliseconds (where X = REFRESH_DELTA).
     */
    private class Refresher extends Thread {

        private volatile boolean stop;

        @Override
        public void run() {
            while (!this.stop) {
                try {
                    updateView();
                    Thread.sleep(REFRESH_DELTA);
                } catch (InterruptedException ex) {
                    showExceptionDialog("InterruptedException", "View refresh has been interrupted");
                }
            }
        }

        /**
         * External command to stop refreshing.
         */
        public void stopRefresh() {
            this.stop = Boolean.TRUE;
        }
    }

}
